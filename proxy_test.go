package socks5

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"strings"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/net/proxy"
)

func TestBadVersion(t *testing.T) {
	assert := assert.New(t)

	pt := &proxyTester{}
	defer pt.Close()

	pt.startSocks()

	c, err := net.Dial("tcp", fmt.Sprintf("localhost:%d", pt.socksPort()))
	assert.NoError(err)
	_, err = c.Write([]byte{255})
	assert.NoError(err)

	buf := make([]byte, 1024)
	_, err = c.Read(buf)
	assert.Equal(io.EOF, err)

}

func TestBadCommandVersion(t *testing.T) {
	assert := assert.New(t)

	pt := &proxyTester{}
	defer pt.Close()

	pt.startSocks()

	c, err := net.Dial("tcp", fmt.Sprintf("localhost:%d", pt.socksPort()))
	assert.NoError(err)

	buf := []byte{
		5, // version
		1, // num auth methods
		0, // auth method "none"

		253, // version. 253 is an invalid value
	}
	_, err = c.Write(buf)
	assert.NoError(err)

	// read auth response
	auth := make([]byte, 2)
	_, err = c.Read(auth)
	assert.NoError(err)

	reply, err := ioutil.ReadAll(c)
	assert.Equal(len(reply), 0)

}

func TestBadCommand(t *testing.T) {
	assert := assert.New(t)

	pt := &proxyTester{}
	defer pt.Close()

	pt.startSocks()

	c, err := net.Dial("tcp", fmt.Sprintf("localhost:%d", pt.socksPort()))
	assert.NoError(err)

	buf := []byte{
		5, // version
		1, // num auth methods
		0, // auth method "none"

		5,   // version.
		252, // command. 253 is an invalid value
	}
	_, err = c.Write(buf)
	assert.NoError(err)

	// read auth response
	auth := make([]byte, 2)
	_, err = c.Read(auth)
	assert.NoError(err)

	reply, err := ioutil.ReadAll(c)
	assert.Equal(len(reply), 0)

}

func TestBadAddressType(t *testing.T) {
	assert := assert.New(t)

	pt := &proxyTester{}
	defer pt.Close()

	pt.startSocks()
	pt.startListen()

	c, err := net.Dial("tcp", fmt.Sprintf("localhost:%d", pt.socksPort()))
	assert.NoError(err)
	addr := []byte("localhost")
	port := make([]byte, 2)
	binary.BigEndian.PutUint16(port, uint16(pt.listenPort()))
	buf := []byte{
		5, // version
		1, // num auth methods
		0, // auth method "none"

		5,   // version
		1,   // command = connect
		0,   // reserved
		254, // address type. 254 is an invalid value
	}
	buf = append(buf, addr...)
	buf = append(buf, port...)
	_, err = c.Write(buf)
	assert.NoError(err)

	// read auth response
	auth := make([]byte, 2)
	_, err = c.Read(auth)
	assert.NoError(err)

	// should close because we send an invalid address type
	reply, err := ioutil.ReadAll(c)
	assert.NoError(err)
	rep, err := parseReply(reply)
	assert.NoError(err)
	assert.Equal(rep.version, 5)
	assert.Equal(rep.rep, repAddressTypeNotSupported)

}

type socksReplyType byte

const (
	repSucceeded                     socksReplyType = 0
	repGeneralSocksFailure           socksReplyType = 1
	repConnectionNotAllowedByRuleset socksReplyType = 2
	repNetworkUnreachable            socksReplyType = 3
	repHostUnreachable               socksReplyType = 4
	repConnectionRefused             socksReplyType = 5
	repTTLExpired                    socksReplyType = 6
	repCommandNotSupported           socksReplyType = 7
	repAddressTypeNotSupported       socksReplyType = 8
)

func parseReply(in []byte) (sr socksReply, err error) {
	if len(in) == 0 {
		err = errors.New("empty socks reply")
		return
	}
	if in[0] != 5 {
		err = errors.New("can only handle version 5")
		return
	}

	sr.version = int(in[0])
	sr.rep = socksReplyType(in[1])
	if in[2] != 0 {
		err = errors.New("unexpected reserved byte value")
		return
	}
	switch in[3] {
	case 1: // ipv4
		ip := net.IP(in[4:8])
		port := binary.BigEndian.Uint16(in[8:10])
		if len(in) != 10 {
			err = errors.New("unexpected length of socks reply")
			return
		}
		sr.address = fmt.Sprintf("%s:%d", ip, port)
	case 4: // ipv6
		ip := net.IP(in[4:20])
		port := binary.BigEndian.Uint16(in[20:22])
		if len(in) != 22 {
			err = errors.New("unexpected length of socks reply")
			return
		}
		sr.address = fmt.Sprintf("%s:%d", ip, port)
	case 3: // domain
		panic("implement me")
	default:
		err = errors.New("unexpected address type")
		return
	}

	return
}

type socksReply struct {
	version int
	rep     socksReplyType
	address string
	port    int
}

func TestUnknownHost(t *testing.T) {
	assert := assert.New(t)

	pt := &proxyTester{}
	defer pt.Close()

	pt.startSocks()

	_, err := pt.dialViaProxy("abcdef-unknown-host.com:80")
	assert.Contains(err.Error(), "host unreachable")
}

func TestConnectionRefused(t *testing.T) {
	assert := assert.New(t)

	pt := &proxyTester{}
	defer pt.Close()

	pt.startSocks()

	_, err := pt.dialViaProxy("localhost:2")
	assert.Contains(err.Error(), "connection refused")
}

func TestConnectName(t *testing.T) {
	assert := assert.New(t)

	pt := &proxyTester{}
	defer pt.Close()

	pt.startListen()
	pt.startSocks()

	con, err := pt.dialViaProxy(fmt.Sprintf("localhost:%d", pt.listenPort()))
	assert.NoError(err)

	if con != nil {
		con.Close()
	}

}

func TestConnectIP4(t *testing.T) {
	assert := assert.New(t)

	pt := &proxyTester{}
	defer pt.Close()

	pt.startListen()
	pt.startSocks()

	con, err := pt.dialViaProxy(fmt.Sprintf("127.0.0.1:%d", pt.listenPort()))
	assert.NoError(err)

	if con != nil {
		con.Close()
	}

}

func TestConnectIP6(t *testing.T) {
	assert := assert.New(t)

	pt := &proxyTester{}
	defer pt.Close()

	pt.startListen()
	pt.startSocks()

	con, err := pt.dialViaProxy(fmt.Sprintf("[::1]:%d", pt.listenPort()))
	assert.NoError(err)

	if con != nil {
		con.Close()
	}
}

func TestUserPassAuthNotImplemented(t *testing.T) {
	assert := assert.New(t)

	pt := &proxyTester{}
	defer pt.Close()

	pt.startSocks()

	con, err := net.Dial("tcp", fmt.Sprintf("localhost:%d", pt.socksPort()))
	if err != nil {
		t.Fatal(err)
	}
	defer con.Close()

	_, err = con.Write([]byte{5, 1, 2})
	if err != nil {
		t.Fatal(err)
	}

	buf := make([]byte, 128)

	i, err := con.Read(buf)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal([]byte{5, 0xff}, buf[0:i])
}

func TestEcho(t *testing.T) {
	assert := assert.New(t)

	pt := &proxyTester{}
	defer pt.Close()

	pt.startListen()

	go func() {
		// echo server
		c := pt.accept()
		buf := make([]byte, 1024)
		for {
			i, err := c.Read(buf)
			if err != nil {
				if err == io.EOF {
					return
				}
				panic(err)
			}
			_, err = c.Write(buf[0:i])
			if err != nil {
				panic(err)
			}
		}
	}()

	// start socks5 server
	pt.startSocks()

	con, err := pt.dialViaProxy(fmt.Sprintf("localhost:%d", pt.listenPort()))
	if err != nil {
		t.Fatal(err)
	}
	defer con.Close()

	msg := "foo bar baz\n"
	if _, err := con.Write([]byte(msg)); err != nil {
		t.Fatal(err)
	}

	response := make([]byte, 1024)
	i, err := io.ReadAtLeast(con, response, len(msg))
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(msg, string(response[0:i]))

}

type proxyTester struct {
	l  net.Listener
	sl net.Listener

	wg sync.WaitGroup
}

func (pt *proxyTester) startListen() {
	var err error
	pt.l, err = net.Listen("tcp", ":0")
	if err != nil {
		panic(err)
	}
}

func (pt *proxyTester) listenPort() int {
	return pt.l.(*net.TCPListener).Addr().(*net.TCPAddr).Port
}

func (pt *proxyTester) Close() {
	if pt.l != nil {
		pt.l.Close()
	}
	if pt.sl != nil {
		pt.sl.Close()
	}
	pt.wg.Wait()
}

func (pt *proxyTester) accept() net.Conn {
	c, err := pt.l.Accept()
	if err != nil {
		panic(err)
	}
	return c
}

func (pt *proxyTester) startSocks() {
	var err error
	pt.sl, err = net.Listen("tcp", ":0")
	if err != nil {
		panic(err)
	}

	ss := NewSocks5Server(pt.sl, (&net.Dialer{}).DialContext)
	pt.wg.Add(1)
	go func() {
		defer pt.wg.Done()
		if err := ss.Serve(); err != nil {
			if strings.Contains(err.Error(), "use of closed") {
				return
			}
			panic(err)
		}
	}()
}

func (pt *proxyTester) socksPort() int {
	return pt.sl.(*net.TCPListener).Addr().(*net.TCPAddr).Port
}

func (pt *proxyTester) dialViaProxy(addr string) (net.Conn, error) {
	// set up socks5 client
	d, err := proxy.SOCKS5("tcp", fmt.Sprintf("localhost:%d", pt.socksPort()), nil, proxy.Direct)
	if err != nil {
		panic(err)
	}

	return d.Dial("tcp", addr)
}
