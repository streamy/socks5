package socks5

import (
	"context"
	"log"
	"net"
	"time"
)

type logRWC struct {
	conn  net.Conn
	label string
}

func (lc *logRWC) Read(data []byte) (int, error) {
	log.Printf("%s : in Read\n", lc.label)
	defer log.Printf("%s : exiting Read\n", lc.label)
	return lc.conn.Read(data)
}

func (lc *logRWC) Write(data []byte) (int, error) {
	log.Printf("%s : in Write\n", lc.label)
	defer log.Printf("%s : exiting Write\n", lc.label)
	return lc.conn.Write(data)
}

func (lc *logRWC) Close() error {
	log.Printf("%s : in Close\n", lc.label)
	defer log.Printf("%s : exiting Close\n", lc.label)
	return lc.conn.Close()
}

func (lc *logRWC) LocalAddr() net.Addr {
	return lc.conn.LocalAddr()
}

func (lc *logRWC) RemoteAddr() net.Addr {
	return lc.conn.RemoteAddr()
}

func (lc *logRWC) SetDeadline(t time.Time) error {
	return lc.conn.SetDeadline(t)
}

func (lc *logRWC) SetReadDeadline(t time.Time) error {
	return lc.conn.SetReadDeadline(t)
}
func (lc *logRWC) SetWriteDeadline(t time.Time) error {
	return lc.conn.SetWriteDeadline(t)
}

func logConnDialFunc(label string, dialFunc func(context.Context, string, string) (net.Conn, error)) func(context.Context, string, string) (net.Conn, error) {
	return func(ctx context.Context, net string, addr string) (net.Conn, error) {
		con, err := dialFunc(ctx, net, addr)
		if err != nil {
			return con, err
		}
		return &logRWC{con, label}, nil
	}
}

type logConnListener struct {
	l     net.Listener
	label string
}

var _ net.Listener = &logConnListener{}

func (lcl *logConnListener) Accept() (net.Conn, error) {
	con, err := lcl.l.Accept()
	if err != nil {
		return con, err
	}
	return &logRWC{con, lcl.label}, nil
}

func (lcl *logConnListener) Close() error {
	return lcl.l.Close()
}

func (lcl *logConnListener) Addr() net.Addr {
	return lcl.Addr()
}
