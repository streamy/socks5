package main

import (
	"log"
	"net"

	"gitlab.com/streamy/socks5"
)

func main() {
	l, err := net.Listen("tcp", ":9999")
	if err != nil {
		log.Fatal(err)
	}
	dialer := net.Dialer{}
	err = socks5.NewSocks5Server(l, dialer.DialContext).Serve()

	log.Fatal(err)
}
