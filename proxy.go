package socks5

import (
	"bufio"
	"bytes"
	"context"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
	"syscall"
)

const debug = false

func NewSocks5Server(listener net.Listener, dialFunc func(ctx context.Context, network, address string) (net.Conn, error)) *socks5server {
	p := &socks5server{listener, dialFunc}

	if debug {
		p.dialFunc = logConnDialFunc("outgoing", p.dialFunc)
		p.l = &logConnListener{p.l, "incoming"}
	}
	return p
}

type socks5server struct {
	l        net.Listener
	dialFunc func(ctx context.Context, network, address string) (net.Conn, error)
}

func (p *socks5server) Serve() error {
	for {
		con, err := p.l.Accept()
		if err != nil {
			return err
		}
		sc := newSocksConn(con, p.dialFunc)
		go sc.handle()
	}
}

func newSocksConn(rwc io.ReadWriteCloser, df func(ctx context.Context, network, address string) (net.Conn, error)) *socksConn {
	sc := &socksConn{
		r:        rwc,
		w:        rwc,
		c:        rwc,
		dialFunc: df,
	}

	// buffer reads
	sc.r = bufio.NewReader(sc.r)

	return sc
}

type socksConn struct {
	r        io.Reader
	w        io.Writer
	c        io.Closer
	dialFunc func(ctx context.Context, network, address string) (net.Conn, error)

	ioError error
}

func (p *socksConn) handle() {
	defer p.c.Close()

	socksVer := p.readByte()
	if int(socksVer) != 5 {
		log.Printf("unhandled socks version %v. closing\n", int(socksVer))
		return
	}
	nmethods := p.readByte()
	methods := p.readBytes(int(nmethods))

	// check for "no auth required"
	if !bytes.Contains(methods, []byte{0}) {
		// client can't handle no authentication.
		p.writeBytes([]byte{5, 0xff})
		return
	}

	p.writeBytes([]byte{5, 0})

	ver := int(p.readByte())
	if ver != 5 {
		log.Println("failed to read expected version from request. Closing")
		return
	}

	cmd := p.readByte()
	var commandFunc func() error
	switch cmd {
	case 1: // CONNECT
		commandFunc = p.connect
	default:
		log.Printf("unknown command %x. Closing\n", cmd)
		return
	}

	rsv := p.readByte()
	if rsv != 0 {
		log.Println("failed to read reserved byte correctly. Closing")
	}
	if p.ioError != nil {
		log.Println("i/o error. closing")
		return
	}

	_ = commandFunc()
	/*
		if err != nil {
			log.Printf("error in connect : %s\n", err.Error())
			return
		}
	*/
}

func doClose(c io.Closer) {
	if err := c.Close(); err != nil {
		log.Printf("error during close : %s\n", err.Error())
	}
}

func (p *socksConn) connect() error {
	atyp := p.readByte()

	var dialAddr string
	switch atyp {
	case 1: // IPv4
		addr := p.readBytes(4)
		port := binary.BigEndian.Uint16(p.readBytes(2))

		dialAddr = fmt.Sprintf("%s:%d", net.IP(addr), port)
	case 4: // IPv6
		addr := p.readBytes(16)
		port := binary.BigEndian.Uint16(p.readBytes(2))

		dialAddr = fmt.Sprintf("[%s]:%d", net.IP(addr), port)
	case 3: // Domain name
		len := int(p.readByte())
		host := p.readBytes(len)
		port := binary.BigEndian.Uint16(p.readBytes(2))

		dialAddr = fmt.Sprintf("%s:%d", host, port)
	default:
		reply(p.w, replyAddressTypeNotSupported)
		return nil
	}

	dialConn, err := p.dialFunc(context.Background(), "tcp", dialAddr)
	switch {
	case err == nil:
		local := dialConn.LocalAddr().(*net.TCPAddr)
		var resp []byte
		if local.IP.To4() == nil {
			// ipv6
			resp = append([]byte{5, 0, 0, 4}, local.IP.To16()...)
			portResp := make([]byte, 2)
			binary.BigEndian.PutUint16(portResp, uint16(local.Port))
			resp = append(resp, portResp...)
		} else {
			//ipv4
			resp = append([]byte{5, 0, 0, 1}, local.IP.To4()...)
			portResp := make([]byte, 2)
			binary.BigEndian.PutUint16(portResp, uint16(local.Port))
			resp = append(resp, portResp...)
		}
		_, err := p.w.Write(resp)
		if err != nil {
			return fmt.Errorf("failed to write response to connect request: %s\n", err.Error())
		}
		errs := make(chan error, 2)
		go func() {
			errs <- doCopy(dialConn, p.r)
		}()
		go func() {
			errs <- doCopy(p.w, dialConn)
		}()

		if err := <-errs; err != nil {
			log.Printf("error in copy: %s\n", err.Error())
		}
		doClose(dialConn)
		if err := <-errs; err != nil {
			log.Printf("error in copy: %s\n", err.Error())
		}

		return nil
	case err != nil:
		reply(p.w, mapError(err))
		return fmt.Errorf("socks5 : %v", err)
	default:
		return fmt.Errorf("socks5 : %v", err)
	}

}

func (p *socksConn) readByte() byte {
	if p.ioError != nil {
		return 0
	}
	buf := []byte{0}
	_, p.ioError = p.r.Read(buf)
	return buf[0]
}

func (p *socksConn) readBytes(len int) (buf []byte) {
	buf = make([]byte, len)
	if p.ioError == nil {
		_, p.ioError = io.ReadFull(p.r, buf)
	}
	return
}

func (p *socksConn) writeBytes(buf []byte) {
	if p.ioError == nil {
		_, p.ioError = p.w.Write(buf)
	}
}

func mapError(err error) byte {
	if oe, ok := err.(*net.OpError); ok {
		switch e := oe.Err.(type) {
		case *net.DNSError:
			return replyHostUnreachable
		case *os.SyscallError:
			switch x := e.Err.(type) {
			case syscall.Errno:
				switch x {
				case syscall.ECONNREFUSED:
					return replyConnectionRefused
				default:
					log.Panicf("Unhandled error: %v\n", x)
				}
			default:
				log.Panicf("Unhandled error:  %T  %v\n", x, x)
			}
		default:
			log.Panicf("unhandled error:  %T  %v\n", e, e)
		}
	}
	return replyGeneralSocksFailure
}

const (
	replySucceeded                     = 0
	replyGeneralSocksFailure           = 1
	replyConnectionNotAllowedByRuleset = 2
	replyNetworkUnreachable            = 3
	replyHostUnreachable               = 4
	replyConnectionRefused             = 5
	replyTTLExpired                    = 6
	replyCommandNotSuported            = 7
	replyAddressTypeNotSupported       = 8
)

func reply(w io.Writer, code byte) error {
	r := []byte{5, code, 0, 1, 0, 0, 0, 0, 0, 0}
	_, err := w.Write(r)
	return err
}

func doCopy(w io.Writer, r io.Reader) error {
	_, err := io.Copy(w, r)
	if err != nil {
		// ignore errors caused by us having called Close()
		if strings.HasPrefix(err.Error(), "read tcp") && strings.Contains(err.Error(), "use of closed network connection") {
			err = nil
		}
	}
	return err
}
